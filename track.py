# Trabalho de FRIA - Felipe Correa, Felipe Borges, Felipe Muriel e Felipe Angelin
# USAGE
# python track.py para webcam :)

# PARA USAR, APERTE z NO TECLADO E SELECIONE OS 4 PONTOS ROI
# AE APERTE Z DE NOVO PRA COMECAR O TRACKING DE FATO
# NOTA - ESTES ALGORITMOS BASEADOS EM HISTOGRAMAS DE CORES PODEM ERRAR NO CRUZAMENTO DE OBJETOS, PORTANTO, TIRE A CARA DO VIDEO!

import numpy as np
import pygame

import cv2

frame = None
roiPts = []
inputMode = False

def update_sprite(pts, window):
	window.fill((0,0,0))
	pygame.draw.rect(window, (255,255,255), [pts[0][0], pts[0][1], 20, 20])
	pygame.display.update()

def selectROI(event, x, y, flags, param):
	global frame, roiPts, inputMode
	# Printa os pontos ROI 
	if inputMode and event == cv2.EVENT_LBUTTONDOWN and len(roiPts) < 4:
		roiPts.append((x, y))
		print x,y
		cv2.circle(frame, (x, y), 4, (0, 255, 0), 2)
		cv2.imshow("frame", frame)

def main():
	global frame, roiPts, inputMode
	
	#pygame window
	# set up pygame
	pygame.init()
	
	#webcam
	camera = cv2.VideoCapture(0)
	
	windowSurface = pygame.display.set_mode((800, 600), 0, 32)
	pygame.display.set_caption('Trabalho de tracking da galerinha da zueira')

	#mouse callback para fazer update dos pontos ROI
	cv2.namedWindow("frame")
	cv2.setMouseCallback("frame", selectROI)

	termination = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 1)
	roiBox = None

	while True:
		# grab the current frame
		(grabbed, frame) = camera.read()

		if roiBox is not None:
			hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
			backProj = cv2.calcBackProject([hsv], [0], roiHist, [0, 180], 1)

			(r, roiBox) = cv2.CamShift(backProj, roiBox, termination)
			pts = np.int0(cv2.cv.BoxPoints(r))
			print 'A mao esta em:', pts
			update_sprite(pts,windowSurface)
			cv2.polylines(frame, [pts], True, (0, 255, 0), 2)

		cv2.imshow("frame", frame)
		key = cv2.waitKey(1) & 0xFF

		# Se apertar z para ir para o modulo de selecao de ROI Points
		if key == ord("z") and len(roiPts) < 4:
			inputMode = True
			orig = frame.copy()
			
			#enquanto nao tiver 4 pontos, loop
			while len(roiPts) < 4:
				cv2.imshow("frame", frame)
				cv2.waitKey(0)

			roiPts = np.array(roiPts)
			s = roiPts.sum(axis = 1)
			tl = roiPts[np.argmin(s)]
			br = roiPts[np.argmax(s)]

			roi = orig[tl[1]:br[1], tl[0]:br[0]]
			roi = cv2.cvtColor(roi, cv2.COLOR_BGR2HSV)

			roiHist = cv2.calcHist([roi], [0], None, [16], [0, 180])
			roiHist = cv2.normalize(roiHist, roiHist, 0, 255, cv2.NORM_MINMAX)
			roiBox = (tl[0], tl[1], br[0], br[1])
		
		# q para sair
		elif key == ord("q"):
			break

	camera.release()
	cv2.destroyAllWindows()

if __name__ == "__main__":
	main()
